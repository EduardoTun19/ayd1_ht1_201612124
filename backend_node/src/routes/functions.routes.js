import { Router } from 'express'


const router = Router()

//-------------------------------------------------- PRINCIPAL FUNCTIONS ---------------------------------------
// get : check if a number is prime or not
router.get('/primo', (req, res) => {
    let body = req.body
    let num = body.num

    // process
    res.send({ 'message': 'success with ' + num })
});

// get : Potencia al cubo de un numero
router.get('/potencia', (req, res) => {
    let body = req.body
    let num = body.num

    let result = Math.pow(num, 3)

    res.send({ 'message': 'EL cubo del numero ' + num + ' es: ' + result })
});

// get : Raiz Cubica de un numero
router.get('/raiz', (req, res) => {
    let body = req.body
    let num = body.num

    let result = Math.pow(num, 1/3)

    res.send({ 'message': 'La raiz cubica del numero ' + num  + ' Es: '+ + result })
});

export default router;