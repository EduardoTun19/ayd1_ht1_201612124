import express from 'express'
import endpoints from './routes/functions.routes.js'
import bodyParser from 'body-parser'
import cors from 'cors'
import morgan from 'morgan'


const app = express()


app.set('port', process.env.PORT || 3000)

app.get('/', (req, res) => {
    res.json({ message: 'hola mundo hotfix'})
    /*name:'Analisis y diseño 1',
    website: 'Practica1 - Grupo 6',
    students:[{carne: '201612282', nombre:'Heidy Carolina Castellanos de Leon'},
    {carne: '201612124', nombre:'Eduardo Saul Tun Aguilar'},
    {carne: '201709140', nombre:'Oscar Armin Crisostomo Ruiz'},
    {carne: '201906552', nombre:'Erick Daniel Antillón Chinchilla'}] })*/
})

var corsOptions = { origin: true, optionsSuccessStatus: 200 }
app.use(cors(corsOptions))
app.use(morgan('dev'))
app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))

app.use('/api', endpoints)

export default app;